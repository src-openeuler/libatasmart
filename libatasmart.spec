Name:           libatasmart
Version:        0.19
Release:        19
Summary:        ATA S.M.A.R.T. Disk Health Monitoring Library
License:        LGPLv2+
URL:            https://github.com/Rupan/libatasmart
Source0:        https://src.fedoraproject.org/repo/pkgs/libatasmart/libatasmart-%{version}.tar.xz/md5/53afe2b155c36f658e121fe6def33e77/libatasmart-%{version}.tar.xz
Patch0:         libatasmart-0.19-wd-fix.patch
BuildRequires:  gcc systemd-devel

%description
a lean, small and clean implementation of an ATA S.M.A.R.T. reading and parsing library.

%package        devel
Summary:        Development Files for libatasmart Client Development
Requires:       %{name} = %{version}-%{release} pkgconfig vala

%description devel
Development Files for libatasmart Client Development

%package_help

%ldconfig_scriptlets

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
%make_install
%delete_la_and_a

%files
%defattr(-,root,root)
%license LGPL
%{_libdir}/*.so.*
%{_sbindir}/*

%files devel
%defattr(-,root,root)
%{_includedir}/atasmart.h
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so
%{_datadir}/vala/*/*.vapi
%exclude %{_docdir}/libatasmart/README

%files help
%defattr(-,root,root)
%doc README */SAMSUNG* */ST* */Maxtor* */WDC* */FUJITSU* */INTEL* */TOSHIBA* */MCC*

%changelog
* Sat Oct 29 2022 wangzhiqiang <wangzhiqiang95@huawei.com> - 0.19-19
- Type:cleancode
- Id:NA
- SUG:NA
- DESC:modify source0 url

* Fri Sep 11 2020 lunankun <lunankun@huawei.com> - 0.19-18
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix source0 url

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.19-17
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file

* Mon Sep 2 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.19-16
- Package init
